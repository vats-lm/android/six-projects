package com.example.vatish.fixedheightrecyclerviewexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private Button mButton;
    private WordListAdapter mAdapter;
    private List<Word> mWords;
    private LinearLayout mRecyclerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.recyclerView);
        mButton = findViewById(R.id.button);
        mRecyclerViewContainer = findViewById(R.id.recyclerViewContainer);

        mWords = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            mWords.add(new Word("Word " + i));
        }

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new WordListAdapter(mWords);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ViewGroup.LayoutParams layoutParams = mRecyclerViewContainer.getLayoutParams();
                layoutParams.height = mRecyclerView.getMeasuredHeight();
                mRecyclerView.setLayoutParams(layoutParams);
                mRecyclerView.setNestedScrollingEnabled(false);
            }
        });
    }
}
