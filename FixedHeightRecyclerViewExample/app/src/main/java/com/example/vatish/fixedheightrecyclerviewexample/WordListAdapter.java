package com.example.vatish.fixedheightrecyclerviewexample;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordListViewHolder> {

    private List<Word> words;

    public WordListAdapter(List<Word> words) {
        this.words = words;
    }

    @NonNull
    @Override
    public WordListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new WordListViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull WordListViewHolder wordListViewHolder, int i) {

        Word word = words.get(i);
        wordListViewHolder.textView.setText(word.getWord());
    }

    @Override
    public int getItemCount() {
        return words.size();
    }

    class WordListViewHolder extends RecyclerView.ViewHolder{

        TextView textView;

        public WordListViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
        }
    }
}
