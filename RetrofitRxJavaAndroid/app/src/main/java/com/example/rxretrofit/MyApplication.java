package com.example.rxretrofit;

import android.app.Application;
import android.content.Context;

import com.example.rxretrofit.di.component.ApplicationComponent;
import com.example.rxretrofit.di.component.DaggerApplicationComponent;
import com.example.rxretrofit.di.module.ApplicationModule;

public class MyApplication extends Application {

    private ApplicationComponent applicationComponent;

    public static MyApplication get(Context context) {
        return (MyApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }
}
