package com.example.rxretrofit.api;


import com.example.rxretrofit.pojo.StoreCoupons;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StoreCouponsApi {

    @GET("coupons/")
    Single<StoreCoupons> getCoupons(@Query("status") String status);

    @GET("storeOffers/")
    Single<StoreCoupons> getStoreInfo();
}
