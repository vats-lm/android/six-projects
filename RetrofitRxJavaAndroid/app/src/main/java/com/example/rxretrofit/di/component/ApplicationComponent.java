package com.example.rxretrofit.di.component;

import com.example.rxretrofit.di.module.RepositoryModule;
import com.example.rxretrofit.di.module.ServiceModule;
import com.example.rxretrofit.di.viewmodel.ViewModelModule;
import com.example.rxretrofit.repository.CouponRepository;
import com.example.rxretrofit.ui.MainActivity;
import com.example.rxretrofit.viewmodel.MainActivityViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ServiceModule.class, RepositoryModule.class, ViewModelModule.class})
public interface ApplicationComponent {

    void inject(CouponRepository couponRepository);

    void inject(MainActivityViewModel mainActivityViewModel);

    void inject(MainActivity mainActivity);
}
