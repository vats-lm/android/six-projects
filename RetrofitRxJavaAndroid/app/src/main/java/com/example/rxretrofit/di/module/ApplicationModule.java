package com.example.rxretrofit.di.module;

import android.content.Context;

import com.example.rxretrofit.MyApplication;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private MyApplication myApplication;

    public ApplicationModule(MyApplication myApplication) {
        this.myApplication = myApplication;
    }

    @Provides
    Context getContext() {
        return myApplication;
    }

    @Provides
    MyApplication getMyApplication() {
        return myApplication;
    }
}
