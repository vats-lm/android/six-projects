package com.example.rxretrofit.di.module;

import com.example.rxretrofit.MyApplication;
import com.example.rxretrofit.repository.CouponRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = ApplicationModule.class)
public class RepositoryModule {

    @Singleton
    @Provides
    CouponRepository getCouponRepository(MyApplication application) {
        return new CouponRepository(application);
    }
}
