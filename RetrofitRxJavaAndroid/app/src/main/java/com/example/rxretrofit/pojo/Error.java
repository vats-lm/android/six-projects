package com.example.rxretrofit.pojo;

public class Error {

    private Throwable error;
    private String message;

    private Error(Throwable error, String message) {

        this.error = error;
        this.message = message;
    }

    public static Error create(Throwable error) {
        return new Error(error, "");
    }

    public static Error create(Throwable error, String message) {
        return new Error(error, message);
    }

    public Throwable getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
