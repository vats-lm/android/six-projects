package com.example.rxretrofit.pojo;

public class Progress {

    private boolean inProgress;
    private String message;

    private Progress(boolean inProgress, String message) {

        this.inProgress = inProgress;
        this.message = message;
    }

    public static Progress start() {
        return new Progress(true, "");
    }

    public static Progress start(String message) {
        return new Progress(true, message);
    }

    public static Progress stop() {
        return new Progress(false, "");
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public String getMessage() {
        return message;
    }
}
