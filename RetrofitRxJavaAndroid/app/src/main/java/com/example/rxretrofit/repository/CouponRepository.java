package com.example.rxretrofit.repository;

import com.example.rxretrofit.MyApplication;
import com.example.rxretrofit.api.StoreCouponsApi;
import com.example.rxretrofit.pojo.StoreCoupons;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CouponRepository {

    @Inject
    StoreCouponsApi storeCouponsApi;

    public CouponRepository(MyApplication application) {
        application.getComponent().inject(this);
    }

    //two Retrofit service calls execute parallel using RxJava
    public Flowable<StoreCoupons> getStoreCouponData() {
        //first it creates an observable which emits retrofit service class
        //to leave current main thread, we need to use subscribeOn which subscribes the observable on computation thread
        //flatMap is used to apply function on the item emitted by previous observable
        //function makes two rest service calls using the give retrofit object for defined api interface
        //these two calls run parallel that is why subscribeOn is used on each of them
        //since these two api call return same object, they are joined using concatArray operator
        //finally consumer observes on android main thread

        return Flowable.just(storeCouponsApi)
                .subscribeOn(Schedulers.computation())
                .flatMap(s -> {
                    Single<StoreCoupons> couponsObservable
                            = s.getCoupons("topcoupons")
                            .subscribeOn(Schedulers.io());

                    Single<StoreCoupons> storeInfoObservable
                            = s.getStoreInfo()
                            .subscribeOn(Schedulers.io());

                    return Single.concatArray(couponsObservable, storeInfoObservable);
                })
                .retry(3)
                .observeOn(AndroidSchedulers.mainThread());
    }

    //single api call using retrofit and rxjava
    public Single<StoreCoupons> getCouponData() {
        return storeCouponsApi
                .getCoupons("topcoupons")
                .subscribeOn(Schedulers.io())
                .retry(3)
                .observeOn(AndroidSchedulers.mainThread());
    }
}
