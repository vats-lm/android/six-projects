package com.example.rxretrofit.ui;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rxretrofit.MyApplication;
import com.example.rxretrofit.R;
import com.example.rxretrofit.pojo.Error;
import com.example.rxretrofit.pojo.Progress;
import com.example.rxretrofit.pojo.StoreCoupons;
import com.example.rxretrofit.viewmodel.MainActivityViewModel;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private RecyclerView couponRecyclerView;
    private MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyApplication.get(this).getComponent().inject(this);

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel.class);

        //set layout manager for recyclerView
        couponRecyclerView = findViewById(R.id.coupon_rv);
        RecyclerView.LayoutManager couponLayoutManager = new LinearLayoutManager(this);
        couponRecyclerView.setLayoutManager(couponLayoutManager);

        // Observe all errors of MainActivityViewModel here...
        viewModel.getErrorLiveData()
                .observe(this, this::handleError);

        // Observe all progress start and stop of MainActivityViewModel here..
        viewModel.getProgressLiveData()
                .observe(this, this::handleProgress);

        // Observe any coupon data changes here..
        viewModel.getCoupons()
                .observe(this, this::handleResults);

        // Observe any store coupon data changes here..
        viewModel.getStoreCoupons()
                .observe(this, this::handleResults);
    }

    // It will refresh coupons from the server and display it to the user.
    public void showCoupons(View view) {
        viewModel.refreshCouponData();
    }

    public void showCouponsTopStore(View view) {
        viewModel.refreshStoreCouponData();
    }

    private void handleResults(StoreCoupons storeCoupons) {
        if (storeCoupons.getCoupons() != null) {
            CouponsAdapter ca = new CouponsAdapter(storeCoupons.getCoupons(), MainActivity.this);
            couponRecyclerView.setAdapter(ca);
        } else {
            TextView storeName = findViewById(R.id.store_name);
            storeName.setText(storeCoupons.getStore());
            TextView couponCount = findViewById(R.id.coupon_count);
            couponCount.setText(storeCoupons.getTotalCoupons());
            TextView maxCashback = findViewById(R.id.max_cashback);
            maxCashback.setText(storeCoupons.getMaxCashback());
        }
    }

    private void handleError(Error error) {
        Throwable t = error.getError();
        Log.d("rx", "" + t.toString());
        Toast.makeText(this, "ERROR IN GETTING COUPONS",
                Toast.LENGTH_LONG).show();
    }

    private void handleProgress(Progress progress) {
        Log.d("rx", "" + progress.getMessage());
        Toast.makeText(this, "Progress: " + progress.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
