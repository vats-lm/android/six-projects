package com.example.rxretrofit.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.rxretrofit.pojo.Error;
import com.example.rxretrofit.pojo.Progress;
import com.example.rxretrofit.pojo.StoreCoupons;
import com.example.rxretrofit.repository.CouponRepository;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class MainActivityViewModel extends ViewModel {

    private CouponRepository repository;
    private MutableLiveData<StoreCoupons> couponsLiveData;
    private MutableLiveData<StoreCoupons> storeCouponsLiveData;
    private MutableLiveData<Progress> progressLiveData;
    private MutableLiveData<Error> errorLiveData;
    private CompositeDisposable compositeDisposable;
    private Disposable couponsDisposable, storeCouponsDisposable;

    @Inject
    public MainActivityViewModel(CouponRepository repository) {
        this.repository = repository;
        couponsLiveData = new MutableLiveData<>();
        storeCouponsLiveData = new MutableLiveData<>();
        progressLiveData = new MutableLiveData<>();
        errorLiveData = new MutableLiveData<>();
        compositeDisposable = new CompositeDisposable();
    }

    public LiveData<StoreCoupons> getCoupons() {
        return couponsLiveData;
    }

    public LiveData<StoreCoupons> getStoreCoupons() {
        return storeCouponsLiveData;
    }

    public LiveData<Progress> getProgressLiveData() {
        return progressLiveData;
    }

    public LiveData<Error> getErrorLiveData() {
        return errorLiveData;
    }

    private void dispose(Disposable disposable) {
        if (disposable != null) {
            if (!disposable.isDisposed()) {
                disposable.dispose();
            }
            compositeDisposable.remove(disposable);
        }
    }

    public void refreshCouponData() {
        dispose(couponsDisposable);
        couponsDisposable = repository.getCouponData()
                .doOnSubscribe(__ -> {
                    progressLiveData.setValue(Progress.start("CouponRepository getCouponData Loading.."));
                })
                .subscribe(res -> {
                            Log.d("rx", "CouponRepository getCouponData Success..");
                            couponsLiveData.setValue(res);
                        }
                        , err -> {
                            errorLiveData.setValue(Error.create(err, "CouponRepository getCouponData Error.."));
                        });
        compositeDisposable.add(couponsDisposable);
    }

    public void refreshStoreCouponData() {
        dispose(storeCouponsDisposable);
        storeCouponsDisposable = repository.getStoreCouponData()
                .doOnSubscribe(__ -> {
                    progressLiveData.setValue(Progress.start("CouponRepository getStoreCouponData Loading.."));
                })
                .subscribe(res -> {
                            Log.d("rx", "CouponRepository getStoreCouponData Success..");
                            storeCouponsLiveData.setValue(res);
                        }
                        , err -> {
                            errorLiveData.setValue(Error.create(err, "CouponRepository getStoreCouponData Error.."));
                        });
        compositeDisposable.add(storeCouponsDisposable);
    }

    @Override
    protected void onCleared() {
        //dispose subscriptions
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            Log.d("rx", "disposables: " + compositeDisposable.size());
            compositeDisposable.clear();
        }
    }
}
