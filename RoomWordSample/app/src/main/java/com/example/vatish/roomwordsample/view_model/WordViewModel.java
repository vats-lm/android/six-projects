package com.example.vatish.roomwordsample.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.vatish.roomwordsample.entity.Word;
import com.example.vatish.roomwordsample.repository.WordRepository;

import java.util.List;

public class WordViewModel extends AndroidViewModel {

    private WordRepository mRepository;
    private LiveData<List<Word>> mWordList;

    public WordViewModel(@NonNull Application application) {
        super(application);

        mRepository = new WordRepository(application);
        mWordList = mRepository.getAllWords();
    }

    public LiveData<List<Word>> getAllWords() {
        return mWordList;
    }

    public void insertWord(Word word) {
        mRepository.insertWord(word);
    }
}
